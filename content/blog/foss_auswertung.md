+++ 
date = "2020-12-01T15:00:00+01:00" 
draft = false 
author = "Anoxinon" 
title = "FOSS-Förderung" 
description = "Auswertung" 
categories = ["Verein", "Open Source"] 
tags = ["Open Source"] 
+++ 

Hallo!

Die Auswertung der Einsendungen für das [FOSS-Projekt](https://anoxinon.de/blog/foss-foerderprojekt/) ist abgeschlossen. 

Die meisten Stimmen hat [F-Droid](https://f-droid.org/) erhalten. 

Den allseits bekannten und beliebten Appstore für Android muss man nicht extra vorstellen. Wer Wert auf open source apps legt, kommt an F-Droid nicht vorbei!

Seit über 10 Jahren werden wir von den fleißigen Entwicklern und Verwaltern mit Apps versorgt. Im Namen von Anoxinon und sicher der ganzen Community, vielen, vielen Dank dafür!

Als kleines Dankeschön spenden wir 500 € + Summe X. Sämtliche Spenden an Anoxinon welche bis zum 15.12.2020 eingehen, lassen wir der F-Droid Crew zukommen. 

Unabhängig von unserer Aktion möchten wir nochmals darauf hinweisen, wie wichtig auch eine regelmäßige finanzielle Unterstützung ist. 

Unser Dank gilt allen Beteiligten an FOSS-Projekten. Danke für eure unermüdliche Arbeit die uns allen zugute kommt!

Team Anoxinon