+++
date = "2018-09-18T17:00:00+01:00"
draft = false
author = "Anoxinon"
title = "September Update"
description = "Messenger, Update zur Vereinsgründung"
categories = ["Allgemeines", "Verein", "Dienste"]
tags = ["Vereinsgründung","anoxinon","fediverse","XMPP", "Website"]
+++
Hallo zusammen,

heute grüßt die Küche mit kurzen und knackigen Neuigkeiten über die Vereinsgründung und den XMPP Server.

---

###### XMPP Server

Wie bereits im August Update angekündigt wurde, werden wir ab Anfang Oktober einen XMPP Server auf Prosody Basis anbieten.
Hierbei legen wir, wie bei allen Diensten, den Fokus auf Datenschutz, Sicherheit und Kompitabilität.

Alle notwendigen Erweiterungen (XEP's) und eine angemessene Uploadbegrenzung für Dateien sind natürlich inkludiert.

Aus diesem Grund läuft derzeit noch eine Testphase (Closed Beta) damit möglichst wenig Probleme im Dauerbetrieb auftreten.
Die Serverkonfiguration erreicht beim XMPP Compliance Test von Conversations.im übrigens satte 100%. An dieser Stelle sei Thomas und Nico
gedankt. Sie haben uns bei unseren Fragen helfend zur Seite gestanden.

---

###### Vereinsgründung

Unser erster Satzungsentwurf wurde vom Finanzamt geprüft und weitestgehend für passend befunden.
Die bemängelten Stellen bessern wir im Moment aus und schicken dann den zweiten Entwurf zur Prüfung ein.
Das ganze kann also noch ein paar Wochen dauern. Da wir als gemeinnütziger Verein anerkannt werden wollen, gehen wir lieber auf Nummer sicher,  um in Zukunft einen reibungsfreien Ablauf zu gewährleisten.
Bis es soweit ist, werden wir weiter an unseren Diensten bzw. Konzepten arbeiten.

In der Zwischenzeit haben sich bereits neue Mitstreiter gefunden, sowohl im technischen Bereich als auch für
die spätere Veröffentlichung/Erarbeitung von Inhalten.

Wir suchen natürlich weiterhin nach interessierten Leuten, die uns fachlich wie inhaltlich unterstützen möchten.  
Schreibt uns doch einfach [hier](https://anoxinon.de/kontakt/) an. :)

---

Soviel dazu für heute, mehr gibt es in einem nachfolgenden Post. ^^

Grüße,

euer Anoxinon Team
