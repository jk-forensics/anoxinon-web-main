+++
date = "2018-10-04T17:00:00+01:00"
author = "Anoxinon"
title = "Eröffnung Anoxinon Messenger"
description = "Start des XMPP Server"
categories = ["Allgemeines", "Verein", "Dienste"]
tags = ["anoxinon","fediverse","XMPP", "Messenger", "Website"]
+++

Hört, Hört werte Leut, wir haben wieder etwas zu verkünden.

Nach intensiver Arbeit sowie einer angemessenen Testphase geht unser XMPP Chat Server Anoxinon.me heute online.
Ab sofort könnt Ihr Euch via Webformular für diesen Service registrieren und ein Nutzerkonto anlegen.

Als Betreiber legen wir viel Wert auf Aktualität, Sicherheit und Datenschutz.
Wir erreichen z. B. mit unserer Server-Konfiguration, beim [Compliance Test](https://compliance.conversations.im/server/anoxinon.me/) von conversation.im, einen Wert 100%. Wir sind stets bemüht, so wenig Daten wie möglich zu sammeln. Dementsprechend fragen wir keine E-Mail Adressen oder ähnliche Informationen bei der Registrierung ab.  

Weitere Informationen dazu könnt Ihr in unserer [Datenschutzerklärung](/datenschutzerklaerungxmppserver/) und in den [Häufigen Fragen](/faq) finden.

Ebenso haben wir einen MUC für User/Interessierte eingerichtet, in dem Ihr Euch mit uns austauschen könnt; Dieser ist unter "anoxinon@conference.anoxinon.me" zu erreichen.

<center>
  <a href="/dienste/anoxinonmessenger">Zur Registration</a>
</center>

Wir hoffen, Euch hiermit einen Mehrwert bieten zu können. XMPP ist vor allem bei Kritikern von Whatsapp, Telegram und ähnlichen Messengern beliebt, weil es eine freie, unabhängige und Datenschutz freundliche Alternative darstellt.

Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Grüße,

euer Anoxinon Team
